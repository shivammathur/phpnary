<?php

namespace Phpnary\Tests\Library;


use Phpnary\Library\Phpnary;

class PhpnaryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Tests method to converts binary string to character string.
     *
     * @author Shivam Mathur <shivam_jpr@hotmail.com>
     * @covers \Phpnary\Library\Phpnary::phpnary()
     */
    public function testPhpnary()
    {
        $this->assertTrue(class_exists('\Phpnary\Library\Phpnary'));
        $phpnary = new Phpnary();
        $phpnary->phpnary('01000010 01101001 01110100 01100011 01101111 01101001 01101110');
        $this->expectOutputString('Bitcoin');
    }

    /**
     * Tests method to converts character string to binary string.
     *
     * @author Shivam Mathur <shivam_jpr@hotmail.com>
     * @covers \Phpnary\Library\Phpnary::reverse_phpnary()
     */
    public function testReversePhpnary()
    {
        $this->assertTrue(class_exists('\Phpnary\Library\Phpnary'));
        $phpnary = new Phpnary();
        $phpnary->reversePhpnary('Bitcoin');
        $this->expectOutputString('01000010 01101001 01110100 01100011 01101111 01101001 01101110');
    }
}
