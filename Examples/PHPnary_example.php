<?php

require __DIR__ . '\..\vendor\autoload.php';

$PHPnary = new PHPnary\Library\PHPnary();

// This echo's 'Bitcoin'
$PHPnary->phpnary('01000010 01101001 01110100 01100011 01101111 01101001 01101110');

// This echo's '01000010 01101001 01110100 01100011 01101111 01101001 01101110'
$PHPnary->reversePhpnary('Bitcoin');
