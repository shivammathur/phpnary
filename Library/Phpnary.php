<?php

namespace Phpnary\Library;


class Phpnary
{
    /**
     * Converts binary string to character string.
     *
     * @author Shivam Mathur <shivam_jpr@hotmail.com>
     *
     * @param $binaryString
     */
    public function phpnary($binaryString)
    {
        $tempArray = explode(' ', trim(stripslashes($binaryString)));
        array_walk($tempArray, function ($binaryChar) {
            echo chr(bindec($binaryChar));
        });
    }

    /**
     * Converts character string to binary string.
     *
     * @author Shivam Mathur <shivam_jpr@hotmail.com>
     *
     * @param $string
     */
    public function reversePhpnary($string)
    {
        $tempString = '';
        $tempArray = str_split(trim(stripslashes($string)));
        array_walk($tempArray, function ($char) use (&$tempString) {
            $tempString .= str_pad(decbin(ord($char)), 8, '0', STR_PAD_LEFT) . ' ';
        });

        echo trim($tempString);
    }
}