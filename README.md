# PHPnary
[![Build Status](https://travis-ci.org/shivammathur/PHPnary.svg?branch=master)](https://travis-ci.org/shivammathur/PHPnary)
[![Code Climate](https://codeclimate.com/github/shivammathur/PHPnary/badges/gpa.svg)](https://codeclimate.com/github/shivammathur/PHPnary)
[![codecov](https://codecov.io/gh/shivammathur/phpnary/branch/master/graph/badge.svg)](https://codecov.io/gh/shivammathur/phpnary)
[![Coverage Status](https://coveralls.io/repos/github/shivammathur/PHPnary/badge.svg?branch=master)](https://coveralls.io/github/shivammathur/PHPnary?branch=master)
[![License](https://poser.pugx.org/shivammathur/PHPnary/license)](https://packagist.org/packages/shivammathur/phpnary)

## I created this repo in order to learn integrating Travis CI and to learn the working of code coverage. I know its dumb.

Convert Binary-to-String and String-to-Binary

- `phpnary()`         - Binary to String
- `reversePhpnary()` - String to Binary

## Installation
- Download this library using [composer](https://getcomposer.org/download/) by executing the command below.
```
composer global require shivammathur/phpnary "master-dev"
```
- Then install the API using by executing the command below.
```
composer create-project shivammathur/phpnary phpnary "master-dev" --prefer-dist
```

## Usage
```php
<?php

require __DIR__ . '\..\vendor\autoload.php';

$PHPnary = new PHPnary\Library\PHPnary();

// This prints 'Bitcoin'
$PHPnary->phpnary('01000010 01101001 01110100 01100011 01101111 01101001 01101110');

// This prints '01000010 01101001 01110100 01100011 01101111 01101001 01101110'
$PHPnary->reversePhpnary('Bitcoin');
```

## Inspiration
This small PHP library was written to find out what is written on T.J. Miller's shirt.

<img align="right" alt="TJ Miller binary shirt" src="http://i.imgur.com/Wx937p6.jpg"/>

The above image is taken from the TV series [Silicon Valley](http://www.hbo.com/silicon-valley).
